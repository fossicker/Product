<?php
// +----------------------------------------------------------------------
// | ThinkCMF [ WE CAN DO IT MORE SIMPLE ]
// +----------------------------------------------------------------------
// | Copyright (c) 2013-2019 http://www.thinkcmf.com All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
// | Author: 老猫 <thinkcmf@126.com>
// +----------------------------------------------------------------------
return [
    'TABLE_PRODUCT_CATEGORY'  => '产品分类',
    'TABLE_PRODUCT_POST'      => '产品',
    'TABLE_SLIDE'      => '幻灯片',
    'TABLE_PRODUCT_POST#PAGE' => '页面'
];