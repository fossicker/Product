<?php
// +----------------------------------------------------------------------
// | ThinkCMF [ WE CAN DO IT MORE SIMPLE ]
// +----------------------------------------------------------------------
// | Copyright (c) 2013-2019 http://www.thinkcmf.com All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
// | Author: 老猫 <thinkcmf@126.com>
// +----------------------------------------------------------------------
namespace app\product\controller;

use cmf\controller\AdminBaseController;

/**
 * Class AdminIndexController
 * @package app\product\controller
 * @adminMenuRoot(
 *     'name'   =>'产品管理',
 *     'action' =>'default',
 *     'parent' =>'',
 *     'display'=> true,
 *     'order'  => 40,
 *     'icon'   =>'th',
 *     'remark' =>'产品管理'
 * )
 */
class AdminIndexController extends AdminBaseController
{


}
